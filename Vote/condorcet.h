 /**
 * @file condorcet.h
 * @author Maxime Tancrède
 * @brief fichier contenant les fonction pour exécuter les algorythme minimax et de schulze
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

 #ifndef CONDORCET_H
 #define CONDORCET_H

 #include "../Util/res.h"
 #include "../Util/ballot.h"
 #include "../Util/duel_mat.h"

/**
 * @brief algorythme minimax
 * 
 * @param[out] resultats résultat a stocker
 * @param[in] mat donner a traiter
 */
 void minimax(res* resultats, duel_mat mat);

/**
 * @brief algorythme schulze
 * 
 * @param[out] resultats résultat a stocker
 * @param[in] mat donner a traiter
 */
 void schulze(res* resultats, duel_mat mat);

 

 #endif