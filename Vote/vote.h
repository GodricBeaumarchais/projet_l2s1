 /**
 * @file vote.h
 * @author Maxime Tancrède
 * @brief execute et stock les resultats de chaque systeme de vote selon les demande de l'utilisateur 
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

 #ifndef VOTE_H
 #define VOTE_H

 #include "../Util/res.h"
 #include "../Util/ballot.h"
 #include "../Util/duel_mat.h"

/**
 * @brief exécute et stocke les resultats des elections selon les parametre entré par l'utilisateur
 * 
 * @param[out] resultats variable ou stocker les resultats
 * @param[in] mat matrice de duel du vote
 * @param[in] bal ballot correspondant au vote (peut ne pas exister)
 * @param[in] ballot_exist donne des information sur l'existance de balot (true si il existe)
 */
 void vote(res* resultats, duel_mat mat, ballot bal, bool ballot_exist);



 #endif