 /**
 * @file uni.h
 * @author Maxime Tancrède
 * @brief fichier contenant les fonction pour exécuter les algorythme pour les scrutins uninominal
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

 #ifndef UNI_H
 #define UNI_H

 #include "../Util/res.h"
 #include "../Util/ballot.h"
 #include "../Util/duel_mat.h"
 #include "../Util/util.h"

 void compte_vote_uni(tab)

/**
 * @brief donne les resultats du scrutin uninominale a un tour
 * 
 * @param[out] resultats résultat a stocker
 * @param[in] bal donner a traiter
 */
 void uni1(res* resultats, ballot bal);

/**
 * @brief donne les resultats du scrutin uninominale a deux tour
 * 
 * @param[out] resultats résultat a stocker
 * @param[in] bal donner a traiter
 */
 void uni2(res* resultats, ballot bal);

 

 #endif