/**
 * @file verif_vote.h
 * @author Maxime Tancrede
 * @brief appelle les fonction affin d'obtenir une liste avec des noms crypté 
 * @version 0.1
 * @date 2021-11-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */


#ifndef VERIF_COTE_H
#define VERIF_COTE_H

#include "./Util/ballot.h"


/**
 * @brief créer la liste crypté des candidat
 * 
 * @param[in] bal ballot contenant le sinformations crypté
 * @param[out] res liste crypté des candidats a remplir
 */
void verif_vote(ballot bal, res_hasher* res);



#endif