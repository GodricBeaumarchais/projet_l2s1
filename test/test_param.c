#include "../Util/parametre.h"


#include <assert.h>
#include <stdio.h>

void test_param(){
    printf("test parametre :\n\n");
    parametre param;
    init_param(&param);
    printf("test param/param_item init :\n");
    assert(!param.i.exist);printf("\tinitialisation de exist reussi\n");
    assert(strcmp(param.i.option.tab,"\0")==0);printf("\tinitialisation tableau réussi\n");
    assert(param.i.option.size ==0);printf("\tinitialisation de la taille réussi\n");

    printf("test param/param_item init :\n");
    assert(!param.i.exist);printf("\tinitialisation de exist reussi\n");
    assert(strcmp(param.i.option.tab,"\0")==0);printf("\tinitialisation tableau réussi\n");
    assert(param.i.option.size ==0);printf("\tinitialisation de la taille réussi\n");

    printf("test est option :\n");
    assert(est_option("-i"));printf("\ttest 1 réussi\n");
    assert(est_option("-d"));printf("\ttest 2 réussi\n");
    assert(est_option("-o"));printf("\ttest 3 réussi\n");
    assert(est_option("-m"));printf("\ttest 4 réussi\n");
    assert(!est_option("-n"));printf("\ttest 5 réussi\n");

    printf("test param format valide :\n");
    const char* tab1[1]={"test"};
    assert(!format_param_valide(1, tab1));printf("\ttest 1 réussi\n");
    const char* tab2[2]={"test", "-i"};
    assert(!format_param_valide(2,tab2));printf("\ttest 2 réussi\n");
    const char* tab3[3]={"test", "-i", "-m"};
    assert(!format_param_valide(3,tab3));printf("\ttest 3 réussi\n");
    const char* tab4[4]={"test", "-i", "oui", "-o"};
    assert(!format_param_valide(4,tab4));printf("\ttest 4 réussi\n");
    const char* tab5[3]={"test", "-i", "hihi"};
    assert(format_param_valide(3,tab5));printf("\ttest 5 réussi\n");

    param_item item;
    init_param_item(&item);
    set_param_item(&item, "oui");
    printf("test set param item :\n");
    assert(item.exist);printf("\ttest 1 réussi\n");
    assert(equal_string_char_tab(item.option, "oui"));printf("\ttest 2 réussi\n");

    printf("test set param :\n");
    init_param(&param);
    const char* tab6[9]={"test", "-i", "arg1", "-d","arg2", "-o","arg3", "-m","arg4"};
    set_param(9,tab6, &param);
    assert(param.i.exist);printf("\ttest 1.1 reussi\n");
    assert(equal_string_char_tab(param.i.option,"arg1"));printf("\ttest 1.2 reussi\n");
    assert(param.d.exist);printf("\ttest 2.1 reussi\n");
    assert(equal_string_char_tab(param.d.option,"arg2"));printf("\ttest 2.2 reussi\n");
    assert(param.o.exist);printf("\ttest 3.1 reussi\n");
    assert(equal_string_char_tab(param.o.option,"arg3"));printf("\ttest 3.2 reussi\n");
    assert(param.m.exist);printf("\ttest 4.1 reussi\n");
    assert(equal_string_char_tab(param.m.option,"arg4"));printf("\ttest 4.2 reussi\n");
        

    printf("test is specified format :\n");
    init_param_item(&item);
    set_param_item(&item, ".txt");
    assert(!is_specified_format(item,".txt"));printf("\ttest 1 reussi\n");
    set_param_item(&item, "oui.txt");
    assert(is_specified_format(item,".txt"));printf("\ttest 2 reussi\n");
    set_param_item(&item, ".txm");
    assert(!is_specified_format(item,".txt"));printf("\ttest 3 reussi\n");
    set_param_item(&item, "ouitxt");
    assert(!is_specified_format(item,".txt"));printf("\ttest 4 reussi\n");
    set_param_item(&item, "oui.csv");
    assert(is_specified_format(item,".csv"));printf("\ttest 5 reussi\n");


    printf("test  is m option :\n");
    init_param_item(&item);
    set_param_item(&item, "all");
    assert(is_m_option(item));printf("\ttest 1 reussi\n");
    set_param_item(&item, "uni1");
    assert(is_m_option(item));printf("\ttest 2 reussi\n");
    set_param_item(&item, "uni2");
    assert(is_m_option(item));printf("\ttest 3 reussi\n");
    set_param_item(&item, "cs");
    assert(is_m_option(item));printf("\ttest 4 reussi\n");
    set_param_item(&item, "cm");
    assert(is_m_option(item));printf("\ttest 5 reussi\n");
    set_param_item(&item, "non");
    assert(!is_m_option(item));printf("\ttest 6 reussi\n");


    printf("test arg param valide :\n");
    init_param(&param);
    const char* tab7[9]={"test", "-i", "arg1.csv", "-d","arg2", "-o","arg3.txt", "-m","all"};
    set_param(9,tab7, &param);
    assert(!argument_param_valide(param));printf("\ttest 1 reussi\n");
    const char* tab8[7]={"test", "-i","arg1.csv", "-o","arg3.xt", "-m","all"};
    init_param(&param);
    set_param(7,tab8, &param);
    assert(!argument_param_valide(param));printf("\ttest 2 reussi\n");
    const char* tab9[5]={"test", "-i", "arg1.csv", "-o","arg3.txt"};
    init_param(&param);
    set_param(5,tab9, &param);
    assert(!argument_param_valide(param));printf("\ttest 3 reussi\n");
    const char* tab10[5]={"test", "-i", "arg1.csv", "-m","all"};
    init_param(&param);
    set_param(5,tab10, &param);
    assert(argument_param_valide(param));printf("\ttest 4 reussi\n");
    const char* tab11[7]={"test", "-d","arg1.csv", "-o","arg3.txt", "-m","all"};
    init_param(&param);
    set_param(7,tab11, &param);
    assert(argument_param_valide(param));printf("\ttest 5 reussi\n");

}