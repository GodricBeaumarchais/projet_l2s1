


#include "../Util/util.h"

#include <assert.h>
#include <stdio.h>


/////////////////////////////////
////// TABLEAU CARACTERE
/////////////////////////////////

void test_char_tab_dyna(){

    printf("test char_tab_dyna :\n\n");
    char_tab_dyna tab;
    
    printf("test init :\n");
    init_char_tab_dyna(&tab);
    assert(strcmp(tab.tab,"\0")==0);printf("\tinitialisation tableau réussi\n");
    assert(tab.size ==0);printf("\tinitialisation de la taille réussi\n");

    printf("test add :\n");
    add_char_tab_dyna(&tab,'c');
    assert(strcmp(tab.tab,"c")==0);printf("\tmodification tableau réussi\n");
    assert(tab.size ==1);printf("\tmofification de la taille réussi\n");

    printf("test string to char tab dyna :\n");
    string_to_char_tab(&tab,"oui");
    assert(strcmp(tab.tab,"oui")==0);printf("\tmodification tableau réussi\n");
    assert(tab.size == strlen("oui"));printf("\tmofification de la taille réussi\n");

    printf("test equal string char tab :\n");
    assert(equal_string_char_tab(tab, "oui"));printf("\tequal test 1 reussi\n");
    assert(!equal_string_char_tab(tab, "non"));printf("\tequal test 2 reussi\n");




}

void test_util(){
    test_char_tab_dyna();
}