var searchData=
[
  ['schulze_0',['schulze',['../structres__s.html#a092363af582f2c01b9c9ed6ebe288173',1,'res_s::schulze()'],['../condorcet_8h.html#ac47ed777aa6033cb26a3f1a5398873c6',1,'schulze():&#160;condorcet.h']]],
  ['schulze_5fexist_1',['schulze_exist',['../structres__s.html#ad1c352889fbf4a5f8a1ffcdbc7d4f98e',1,'res_s']]],
  ['score_2',['score',['../structres__item__s.html#ad1e2d046b8008f11a7dd8be9262f8fd4',1,'res_item_s']]],
  ['set_5fballot_3',['set_ballot',['../ballot_8h.html#ac9863d31c33e777594436edfbed97645',1,'ballot.h']]],
  ['set_5fballot_5fitem_4',['set_ballot_item',['../ballot_8h.html#adcd69be74bd9306e4538e4a117b171ce',1,'ballot.h']]],
  ['set_5fduel_5fmat_5fcsv_5',['set_duel_mat_csv',['../duel__mat_8h.html#a55b2f283f6ae3e6d302ead0bddf9cc64',1,'duel_mat.h']]],
  ['set_5fparam_6',['set_param',['../parametre_8h.html#a27be03f04b1ccc748d3dd564ef8365ff',1,'parametre.h']]],
  ['set_5fres_5fitem_7',['set_res_item',['../res_8h.html#abefa8ed3fbf5de0a3e4a294b8aee6c5d',1,'res.h']]],
  ['set_5fres_5fminimax_8',['set_res_minimax',['../res_8h.html#a02f36bb3fef47cdcda6839c2e9e87eec',1,'res.h']]],
  ['set_5fres_5fschulze_9',['set_res_schulze',['../res_8h.html#a4076eb68203e376cb8fedf3a6b163d08',1,'res.h']]],
  ['set_5fres_5funi1_10',['set_res_uni1',['../res_8h.html#a53b4dbd4d1a7c8b4877ae2d0a16d89ba',1,'res.h']]],
  ['set_5fres_5funi2_11',['set_res_uni2',['../res_8h.html#ac6cacda3ad07fd9ff97df092ccd40f6a',1,'res.h']]],
  ['size_12',['size',['../structint__tab__dyna__s.html#a51f528645a3e50c28fd83c722891d3e9',1,'int_tab_dyna_s::size()'],['../structchar__tab__dyna__s.html#a9fed8cd61ba6627a1257974bdefc6e22',1,'char_tab_dyna_s::size()']]]
];
