var searchData=
[
  ['id_0',['id',['../structballot__item__s.html#a64adc362eee96e10db92425f3b59c5b0',1,'ballot_item_s']]],
  ['init_5fballot_1',['init_ballot',['../ballot_8h.html#abafb41ed97e9b9a8f4e58b5c02f95a87',1,'ballot.h']]],
  ['init_5fchar_5fmat_5fdyna_2',['init_char_mat_dyna',['../util_8h.html#a37d357ef9335add9b4a111328d23904b',1,'util.h']]],
  ['init_5fchar_5ftab_5fdyna_3',['init_char_tab_dyna',['../util_8h.html#ac909b8dbae2bb12993f51373b7290ce1',1,'util.h']]],
  ['init_5fint_5fmat_5fdyna_4',['init_int_mat_dyna',['../util_8h.html#af02a59671363c1a031119759ac58756a',1,'util.h']]],
  ['init_5fint_5ftab_5fdyna_5',['init_int_tab_dyna',['../util_8h.html#a5be3a7999ef8a8afe8fc68ad3dbb9bfc',1,'util.h']]],
  ['int_5fmat_5fdyna_5fs_6',['int_mat_dyna_s',['../structint__mat__dyna__s.html',1,'']]],
  ['int_5ftab_5fdyna_7',['int_tab_dyna',['../util_8h.html#afde53d02d4a61d2206077c11f222be04',1,'util.h']]],
  ['int_5ftab_5fdyna_5fs_8',['int_tab_dyna_s',['../structint__tab__dyna__s.html',1,'']]]
];
