var ballot_8h =
[
    [ "ballot_item_s", "structballot__item__s.html", "structballot__item__s" ],
    [ "ballot_s", "structballot__s.html", "structballot__s" ],
    [ "tab_ballot_s", "structtab__ballot__s.html", "structtab__ballot__s" ],
    [ "ballot", "ballot_8h.html#a0313ae947feb0e3fafbf07ce60097543", null ],
    [ "ballot_item", "ballot_8h.html#afa92879cd000c1e50adcc5e141a56416", null ],
    [ "tab_ballot", "ballot_8h.html#a966af887224857f05b173bb3797d2697", null ],
    [ "add_tab_ballot", "ballot_8h.html#a7aba9ba9caf0cf5ce9d3be0cf62a570e", null ],
    [ "check_ballot", "ballot_8h.html#a5b2f10d2d60db6a50d1e92dd12d08409", null ],
    [ "init_ballot", "ballot_8h.html#abafb41ed97e9b9a8f4e58b5c02f95a87", null ],
    [ "remove_tab_ballot", "ballot_8h.html#a86938c6b18e53ef1ad4a10d591e90025", null ],
    [ "set_ballot", "ballot_8h.html#ac9863d31c33e777594436edfbed97645", null ],
    [ "set_ballot_item", "ballot_8h.html#adcd69be74bd9306e4538e4a117b171ce", null ]
];