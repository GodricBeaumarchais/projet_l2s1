var util_8h =
[
    [ "int_tab_dyna_s", "structint__tab__dyna__s.html", "structint__tab__dyna__s" ],
    [ "char_tab_dyna_s", "structchar__tab__dyna__s.html", "structchar__tab__dyna__s" ],
    [ "int_mat_dyna_s", "structint__mat__dyna__s.html", "structint__mat__dyna__s" ],
    [ "char_mat_dyna_s", "structchar__mat__dyna__s.html", "structchar__mat__dyna__s" ],
    [ "char_tab_dyna", "util_8h.html#aac4db3efa910b15c4baf49374a748058", null ],
    [ "int_tab_dyna", "util_8h.html#afde53d02d4a61d2206077c11f222be04", null ],
    [ "add_char_mat_dyna", "util_8h.html#ac1fb57ef6f75174a53993438da1acd06", null ],
    [ "add_char_tab_dyna", "util_8h.html#a040da3a72bc89886a41b12e279d8b936", null ],
    [ "add_int_mat_dyna", "util_8h.html#a5d980d684d35c2e77b898edd7fec80bd", null ],
    [ "add_int_tab_dyna", "util_8h.html#aea224923012fc5d73e3a8fd1a1594f0d", null ],
    [ "init_char_mat_dyna", "util_8h.html#a37d357ef9335add9b4a111328d23904b", null ],
    [ "init_char_tab_dyna", "util_8h.html#ac909b8dbae2bb12993f51373b7290ce1", null ],
    [ "init_int_mat_dyna", "util_8h.html#af02a59671363c1a031119759ac58756a", null ],
    [ "init_int_tab_dyna", "util_8h.html#a5be3a7999ef8a8afe8fc68ad3dbb9bfc", null ],
    [ "remove_char_mat_dyna", "util_8h.html#ac8bd50fc021b42ea1ee72dc770219c1d", null ],
    [ "remove_char_tab_dyna", "util_8h.html#afe26d5c062a532b2093560d34ff4a721", null ],
    [ "remove_int_mat_dyna", "util_8h.html#aea453b4bd595a45c3aff49801c7c45e5", null ],
    [ "remove_int_tab_dyna", "util_8h.html#a1704db25d3b7d2679720c3d7bc500947", null ]
];