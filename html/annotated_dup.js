var annotated_dup =
[
    [ "ballot_item_s", "structballot__item__s.html", "structballot__item__s" ],
    [ "ballot_s", "structballot__s.html", "structballot__s" ],
    [ "char_mat_dyna_s", "structchar__mat__dyna__s.html", "structchar__mat__dyna__s" ],
    [ "char_tab_dyna_s", "structchar__tab__dyna__s.html", "structchar__tab__dyna__s" ],
    [ "duel_mat_s", "structduel__mat__s.html", "structduel__mat__s" ],
    [ "int_mat_dyna_s", "structint__mat__dyna__s.html", "structint__mat__dyna__s" ],
    [ "int_tab_dyna_s", "structint__tab__dyna__s.html", "structint__tab__dyna__s" ],
    [ "parametre_s", "structparametre__s.html", "structparametre__s" ],
    [ "res_item_s", "structres__item__s.html", "structres__item__s" ],
    [ "res_s", "structres__s.html", "structres__s" ],
    [ "res_uni2_s", "structres__uni2__s.html", "structres__uni2__s" ],
    [ "tab_ballot_s", "structtab__ballot__s.html", "structtab__ballot__s" ]
];