var res_8h =
[
    [ "res_item_s", "structres__item__s.html", "structres__item__s" ],
    [ "res_uni2_s", "structres__uni2__s.html", "structres__uni2__s" ],
    [ "res_s", "structres__s.html", "structres__s" ],
    [ "res", "res_8h.html#ab4646d4effb580f9b7a0d45aa4739ab1", null ],
    [ "res_item", "res_8h.html#a67a222f3e7f049bb158c36770d29c635", null ],
    [ "res_uni2", "res_8h.html#a2d6bff5837448d2d2219959ad6841f7a", null ],
    [ "res_init", "res_8h.html#a99d9f6002c0e52610c2f08d0eac0fe5e", null ],
    [ "set_res_item", "res_8h.html#abefa8ed3fbf5de0a3e4a294b8aee6c5d", null ],
    [ "set_res_minimax", "res_8h.html#a02f36bb3fef47cdcda6839c2e9e87eec", null ],
    [ "set_res_schulze", "res_8h.html#a4076eb68203e376cb8fedf3a6b163d08", null ],
    [ "set_res_uni1", "res_8h.html#a53b4dbd4d1a7c8b4877ae2d0a16d89ba", null ],
    [ "set_res_uni2", "res_8h.html#ac6cacda3ad07fd9ff97df092ccd40f6a", null ]
];