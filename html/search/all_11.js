var searchData=
[
  ['vainqueur_0',['vainqueur',['../structres__item__s.html#ae09c9c3cd5a9b8e4f930af0aeb3d41f0',1,'res_item_s']]],
  ['verif_5fvote_1',['verif_vote',['../verif__vote_8h.html#aacb05ce7caff79c52074e37f6c5eeaf0',1,'verif_vote.h']]],
  ['verif_5fvote_2eh_2',['verif_vote.h',['../verif__vote_8h.html',1,'']]],
  ['votant_3',['votant',['../structballot__item__s.html#a01ad623a61ae50d5e44a13d4abfa60ff',1,'ballot_item_s']]],
  ['vote_4',['vote',['../structballot__item__s.html#a488ec0f4cab79ad54a24e2c76007f86d',1,'ballot_item_s::vote()'],['../structtab__ballot__s.html#ab58f9d4eedf444f78f158719e78d3fe6',1,'tab_ballot_s::vote()'],['../structduel__mat__s.html#a71e540694762789c3c82cc68a0a33aac',1,'duel_mat_s::vote()'],['../vote_8h.html#a179abb30ef54ba4db057663397d9613e',1,'vote(res *resultats, duel_mat mat, ballot bal, bool ballot_exist):&#160;vote.h']]],
  ['vote_2eh_5',['vote.h',['../vote_8h.html',1,'']]],
  ['votes_6',['votes',['../structballot__s.html#a9932ec96a3d514efbe1af1767cfcff49',1,'ballot_s']]]
];
