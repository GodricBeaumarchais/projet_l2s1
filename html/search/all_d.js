var searchData=
[
  ['remove_5fchar_5fmat_5fdyna_0',['remove_char_mat_dyna',['../util_8h.html#ac8bd50fc021b42ea1ee72dc770219c1d',1,'util.h']]],
  ['remove_5fchar_5ftab_5fdyna_1',['remove_char_tab_dyna',['../util_8h.html#afe26d5c062a532b2093560d34ff4a721',1,'util.h']]],
  ['remove_5fint_5fmat_5fdyna_2',['remove_int_mat_dyna',['../util_8h.html#aea453b4bd595a45c3aff49801c7c45e5',1,'util.h']]],
  ['remove_5fint_5ftab_5fdyna_3',['remove_int_tab_dyna',['../util_8h.html#a1704db25d3b7d2679720c3d7bc500947',1,'util.h']]],
  ['remove_5ftab_5fballot_4',['remove_tab_ballot',['../ballot_8h.html#a86938c6b18e53ef1ad4a10d591e90025',1,'ballot.h']]],
  ['res_5',['res',['../res_8h.html#ab4646d4effb580f9b7a0d45aa4739ab1',1,'res.h']]],
  ['res_2eh_6',['res.h',['../res_8h.html',1,'']]],
  ['res_5finit_7',['res_init',['../res_8h.html#a99d9f6002c0e52610c2f08d0eac0fe5e',1,'res.h']]],
  ['res_5fitem_8',['res_item',['../res_8h.html#a67a222f3e7f049bb158c36770d29c635',1,'res.h']]],
  ['res_5fitem_5fs_9',['res_item_s',['../structres__item__s.html',1,'']]],
  ['res_5fs_10',['res_s',['../structres__s.html',1,'']]],
  ['res_5funi2_11',['res_uni2',['../res_8h.html#a2d6bff5837448d2d2219959ad6841f7a',1,'res.h']]],
  ['res_5funi2_5fs_12',['res_uni2_s',['../structres__uni2__s.html',1,'']]]
];
