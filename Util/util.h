/**
 * @file util.h
 * @author Maxime Tancrède
 * @brief contiens toutes les structure et fonction non spécifique et diversement utiles dans le programe
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef UTIL_H
#define UTIL_H
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
/**
 * @brief permet de gérer facilement des tableau dynamique d'entier
 * 
 */
typedef struct int_tab_dyna_s{
    int* tab;/**< tableau d'entier */
    int size;/**< taille du tableau*/

} int_tab_dyna;

/**
 * @brief permet de gérer facilement des tableau dynamique de caractère
 * 
 */
typedef struct char_tab_dyna_s{
    char* tab;/**< tableau de caractère*/
    int size;/**< taille du tableau*/
} char_tab_dyna;

/**
 * @brief permet de gérer facilement des matrice dynamique d'entier
 * 
 */
typedef struct int_mat_dyna_s{
    int_tab_dyna* mat;/**< matrice d'entier*/
    int colonne, ligne;/**< dimentions de la matrice*/

}int_mat_dyna;

/**
 * @brief permet de gérer facilement des matrice dynamique de caractère
 * 
 */
typedef struct char_mat_dyna_s{
    char_tab_dyna* mat;/**< matrice de caractère */
    int len;/**< dimentions de la matrice*/

}char_mat_dyna;


/////////////////////////////////
////// TABLEAU ENTIER
/////////////////////////////////
/**
 * @brief ajoute un element a un tableau dynamique d'intager
 * 
 * @param[out] tab tableau a modifier
 * @param[in] x element a ajouter
 */
void add_int_tab_dyna( int_tab_dyna *tab, int x);

/**
 * @brief initialise un tableau d'entier
 * 
 * @param[out] tab tableau a initialiser
 */
void init_int_tab_dyna( int_tab_dyna *tab);

/**
 * @brief retire un element d'un tableau d'entier
 * 
 * @param[out] tab tableau à modifié
 * @param[in] i indice de l'element a supprimer
 */
void remove_int_tab_dyna( int_tab_dyna *tab, int i);

/**
 * @brief libère la mémoire dynamique contenue dans la structure
 * 
 * @param tab structure sur laquel agir
 */
void free_int_tab_dyna(int_tab_dyna *tab);
/////////////////////////////////
////// MATRICE ENTIER
/////////////////////////////////
/**
 * @brief ajoute une ligne a une matrice d'entier
 * 
 * @param[out] mat matrice a modifier
 * @param[in] tab element a ajouter
 */
void add_int_mat_dyna( int_mat_dyna *mat, int *tab);

/**
 * @brief initialise une matrice d'entier
 * 
 * @param[out] mat matrice a initialiser
 */
void init_int_mat_dyna( int_mat_dyna *mat);

/**
 * @brief retire une ligne d'une matrice d'entier
 * 
 * @param[out] mat matrice à modifié
 * @param[in] i indice de l'element a supprimer
 */
void remove_int_mat_dyna( int_mat_dyna *mat, int i);

/////////////////////////////////
////// MATRICE CARACTERE
/////////////////////////////////

/**
 * @brief ajoute une ligne a une matrice dde caractère
 * 
 * @param[out] mat matrice a modifier
 * @param[in] tab element a ajouter
 */
void add_char_mat_dyna( char_mat_dyna *mat, int *tab);

/**
 * @brief initialise une matrice de caractère
 * 
 * @param[out] mat matrice a initialiser
 */
void init_char_mat_dyna( char_mat_dyna *mat);

/**
 * @brief retire une ligne d'une matrice de caractere
 * 
 * @param[out] mat matrice à modifié
 * @param[in] i indice de l'element a supprimer
 */
void remove_char_mat_dyna( char_mat_dyna *mat, int i);



/////////////////////////////////
////// TABLEAU CARACTERE
/////////////////////////////////
/**
 * @brief ajoute un element a un tableau dynamique de caractère
 * 
 * @param[out] tab tableau a modifier
 * @param[in] c element a ajouter
 */
void add_char_tab_dyna( char_tab_dyna *tab, char c);

/**
 * @brief initialise un tableau de caractère
 * 
 * @param[out] tab tableau a initialiser
 */
void init_char_tab_dyna(char_tab_dyna *tab);

/**
 * @brief 
 * 
 * @param tab 
 */
void free_char_tab_dyna(char_tab_dyna *tab);

/**
 * @brief traduit une chaine de caractère en tableau dynamique d'entier
 * 
 * @param[out] tab tableau auquel affecter les valeurs
 * @param[in] str chaine de caractère a copier
 */
void string_to_char_tab(char_tab_dyna *tab, char* str);

/**
 * @brief egualité entr un string et un char tab dyna
 * 
 * @param tab tableau a comparer
 * @param str string a comparer
 * @return true si ils sont identique
 * @return false si ils sont différent
 */
bool equal_string_char_tab(char_tab_dyna tab, char* str);

/**
 * @brief retire un element d'un tableau de caractère
 * 
 * @param[out] tab tableau à modifié
 * @param[in] i indice de l'element a supprimer
 */
void remove_char_tab_dyna( char_tab_dyna* tab, int i);

#endif