/**
 * @file ballot.h
 * @author Maxime Tancrède
 * @brief définitions des tructures pour stocker les données des ballot et fonction pour les éditer
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
*/



#ifndef BALLOT_H
#define BALLOT_H

#define DATE_SIZE
#include <stdbool.h>
#include "util.h"

/**
 * @brief contiens toutes les informations sur le vote d'une personne
 *
 */
typedef struct ballot_item_s {
    int id; /**< id de l'electeur */ 
    int code; /**< code de l'electeur */ 
    char date[DATE_SIZE]; /**< date de l'election */
    char_tab_dyna votant; /**< nom du votant*/
    int* vote; /**< vote de l'electeur */
} ballot_item;

/**
 * @brief contiens toutes les informations d'un ballot
 * 
 */
typedef struct ballot_s{
    char_mat_dyna candidat; /**< contiens la liste des candidat*/
    tab_ballot votes; /**< sontiens la liste de votes*/
}ballot;

/**
 * @brief liste des votes du ballot
 * 
 */
typedef struct tab_ballot_s{
    ballot_item* vote; /**< liste des votes */
    int len; /**< taille de la liste*/
}tab_ballot;

/** 
 * @brief assigne les valeurs au ballot ( peut se passer de init_ballot )
 * 
 * @param[out] bal ballot a modifier
 * @param[in] ligne ligne d'un csv de type ballot 
 */
void set_ballot(ballot* bal, char_tab_dyna_s ligne);

/**
 * @brief initialise un intem de ballot vide 
 * 
 * @param[in] bal ballot a initialiser
 */
void init_ballot(ballot* bal);

/**
 * @brief assigne des valeurs a un item de ballot
 * 
 * @param[out] item item auquel assigné la valeur
 * @param[in] ligne ligne du fichier csv dont les données deuvent être stocker ( ! pas la première) 
 */
void set_ballot_item( ballot_item* item, char_tab_dyna_s ligne)

/**
 * @brief initialise un tableau de ballot_item
 * 
 * @param[out] tab tableau a initialiser
 */
void init_tab_ballot(tab_ballot* tab);

/**
 * @brief ajoute un ballot_item a un tableau
 * 
 * @param[out] tab tableau a incrémenter
 * @param[in] item item à ajouter
 */
void add_tab_ballot( tab_ballot* tab, ballot_item item);

/**
 * @brief suprime un element d'un tableau d'item
 * 
 * @param[out] tab tableau à modifier
 * @param[in] i indice de l'element a supprimer
 */
void remove_tab_ballot( tab_ballot* tab, int i);

/**
 * @brief modifie et supprime les valeurs aberrante du ballot
 * 
 * @param[out] bal balot a modifier
 */
void check_ballot(ballot* bal );

#endif

