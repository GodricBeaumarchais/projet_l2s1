/**
 * @file duel_mat.h
 * @author Maxime Tancrède
 * @brief structure pour stocker les information des matrice de duel et fontion pour agire dessus
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef DUEL_MAT_H
#define DUEL_MAT_H

#include <stdbool.h>
#include "util.h"
#include "ballot.h"


/**
 * @brief structure représentant une matrice de duel
 * 
 */
typedef struct duel_mat_s{
    char_mat_dyna candidat;/**< liste des candidat*/
    int_mat_dyna vote; /**< matrice des votes*/

}duel_mat;

/**
 * @brief affecte des valeurs aux matrices de duel depuis un fichier csv
 * 
 * @param[out] mat matrice a définir
 * @param[in] csv fichier contenant les donner a stocker
 */
void set_duel_mat_csv( duel_mat* mat, char_tab_dyna csv);

/**
 * @brief affecte des valeurs aux matrices de duel depuis un ballot
 * 
 * @param[out] mat matrice a définir
 * @param[in] bal ballot contenant les donner a stocker
 */
void set_duel_mat_ballot( duel_mat* mat, ballot bal)



#endif

