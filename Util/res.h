/**
 * @file res.h
 * @author Maxime Tancrède
 * @brief structures pour stocker les résultats et fonction pour agir dessus
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef RES_H
#define RES_H


/**
 * @brief structure stockant le résultat typique par un system de vote
 * 
 */
typedef struct res_item_s{
    int score; /**< score du candidat après dépouillement, n'étant util que pour les uninominal il sera egal a -1 pour la methode de condorcet*/
    char_tab_dyna vainqueur;/**< vainquer du vote*/
}res_item;

/**
 * @brief structure stockant les résultats d'un vote uninominal a deux tour
 * 
 */
typedef struct res_uni2_s{
    bool majorite;/**< est true si il y a un vainqueur par majoritée absolue*/
    res_item pre_tour1; /**< chorespond au resultat du vainquer du premier tour */
    res_item sec_tour1;  /**< chorespond au resultat du second du premier tourS*/
    res_item gagnant; /**< chorespond au resultat du gagant*/
}res_uni2;


/**
 * @brief structure stockant tout les resultats des algorythme
 * 
 */
typedef struct res_s{
    int nb_candidats;/**< nombre de candidat*/
    int nb_votants;/**< nombre de votant*/
    bool uni1_esxist;/**< confirme l'existance du scrutin uni a un tour*/
    bool uni2_exist;/**< confirme l'existance du scrutin uni a deux tour*/
    bool minimax_exist;/**< confirme l'existance du sructin par condorcet minimax*/
    bool schulze_exist;/**< confirme l'existance du sructin par condorcet schuzle*/
    res_item uni1;/**< resultat du scrutin uninominal a 1 tour*/
    res_item minimax;/**< resultat du sructin par condorcet minimax*/
    res_item schulze; /**< resultat du sructin par condorcet schuzle*/
    res_uni2 uni2;/**< resultat du test uninominal a deux tours*/
}res;

/**
 * @brief assignes des valeurs a un res_item
 * 
 * @param[out] item objet auquel assigner des valeurs 
 * @param[in] nb_candidats nombre de candidat participant au vote
 * @param[in] nb_votants nombre de votant
 * @param[in] score score du gagnat de l'election
 * @param[in] uni true si le vote est uninominal
 * @param[in] gagnant gagnant de l'election
 */
void set_res_item( res_item* item, int nb_candidats, int nb_votants, int score, bool uni, char* gagnant);

/**
 * @brief initialise un resultat !!! on est obligé de l'initialisé prélablement pour le lire
 * 
 * @param[out] resulatats objet a initialisé
 */
void res_init(res* resulatats);

/**
 * @brief assigne un uni1 au resulats en respectant les format
 * 
 * @param[out] resulatats resultat auquel assigné des valurs 
 * @param[in] item item contenant les valeurs du resultat
 */
void set_res_uni1(res* resulatats, res_item item);

/**
 * @brief assigne un uni2 au resulats en respectant les format
 * 
 * @param[out] resulatats resultat auquel assigné des valurs 
 * @param[in] item item contenant les valeurs du resultat
 */
void set_res_uni2(res* resulatats, res_uni2 item);

/**
 * @brief assigne un minimax au resulats en respectant les format
 * 
 * @param[out] resulatats resultat auquel assigné des valurs 
 * @param[in] item item contenant les valeurs du resultat
 */
void set_res_minimax(res* resulatats, res_item item);

/**
 * @brief assigne un schulze au resulats en respectant les format
 * 
 * @param[out] resulatats resultat auquel assigné des valurs 
 * @param[in] item item contenant les valeurs du resultat
 */
void set_res_schulze(res* resulatats, res_item item);


#endif