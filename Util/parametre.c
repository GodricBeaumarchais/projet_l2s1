#include "parametre.h"

void erreur_format_param(){
    printf("Erreure dans les paramètres saisie, le format n'est pas valide");
    exit(2);
}

void erreur_arg_saisie(){
    printf("Erreure dans les paramètres saisie, les argument ne sont pas valide");
    exit(3);
}

/////////////////////////////////
/// fonction sur les structure///
/////////////////////////////////

//  INIT
/////////////////

void set_param(int argc,const char* argv[], parametre *param){
    for(int i = 1; i < argc; i+=2){
        if( strcmp(argv[i], "-i") == 0){
            set_param_item(&param->i, argv[i+1]);
        }
        else if( strcmp(argv[i], "-d") == 0){
            set_param_item(&param->d, argv[i+1]);
        }
        else if( strcmp(argv[i], "-o") == 0){
            set_param_item(&param->o, argv[i+1]);
        }
        else if( strcmp(argv[i], "-m") == 0){
            set_param_item(&param->m, argv[i+1]);
        }
    }
}

void init_param( parametre* param ){
    init_param_item(&param->i);
    init_param_item(&param->d);
    init_param_item(&param->o);
    init_param_item(&param->m);
}


void init_param_item(param_item *item){
    item->exist = false;
    init_char_tab_dyna(&item->option);
}



void set_param_item(param_item *item,const char* option){
    item->exist = true;
    char* chaine = NULL;
    chaine=(char*)option;
    string_to_char_tab(&item->option, chaine);

}

bool option_exist(param_item item){
    if(item.exist)return true;
    return false;
}

const char* get_string_option(param_item item){
    return item.option.tab;
}


/////////////////////////////////////
///////verification des parametre
/////////////////////////////////////


bool est_option(const char* str){
    if(strcmp(str, "-i") == 0|| strcmp(str, "-m")==0 || strcmp(str, "-o")==0 || strcmp(str, "-d")==0)return true;
    return false;
}

bool format_param_valide(int argc,const char* argv[]){
    if(argc%2 == 0 || argc == 1){
        return false;
    }
    for(int i = 1; i < argc; i+=2){
        if(!est_option(argv[i]) || est_option(argv[i+1])){
            return false;
        }
    }
    return true;
}

bool is_specified_format(param_item item, char* extension){
    int len = strlen(extension);
    if( item.option.size < len+1)return false;
    int j = 0;
    for(int i = item.option.size-len; i < item.option.size; i++){
        if(extension[j]!= item.option.tab[i])return false;
        j++;
    }
    return true;
}

bool is_m_option(param_item item){
    
    if(equal_string_char_tab(item.option, "all")) return true;
    else if(equal_string_char_tab(item.option, "uni1")) return true;
    else if(equal_string_char_tab(item.option, "uni2")) return true;
    else if(equal_string_char_tab(item.option, "cm")) return true;
    else if(equal_string_char_tab(item.option, "cs")) return true;
    else return false;
}

bool argument_param_valide(parametre param){
    if(!(option_exist(param.i) ^ option_exist(param.d))){
        
        return false;
    }
    else if(option_exist(param.i)){
        if(is_specified_format(param.i,".csv")){
            if(access(get_string_option(param.i), F_OK|R_OK)!=0){
                printf("oui\n");
                return false;
            }
        }
        else return false;

    }
    else {
        if(is_specified_format(param.d,".csv")){
            if(access(get_string_option(param.d), F_OK|R_OK)!=0){
                return false;
            }
        }
        else return false;
    }
    if(option_exist(param.o)){
        if(!is_specified_format(param.o,".txt")){
            return false;
        }
    }
    if(option_exist(param.m)){
        return is_m_option(param.m);
    }
    else return false;

}




