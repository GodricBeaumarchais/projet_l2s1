/**
 * @file parametre.h
 * @author Maxime Tancrède
 * @brief structures pour stocker les parametres et fonctions pour agir dessus
 * @version 0.1
 * @date 2021-11-14
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef PARAMETRE_H
#define PARAMETRE_H

#include <stdbool.h>
#include "util.h"
#include <string.h>
#include <unistd.h>
#include <stdlib.h>



/**
 * @brief contiens les données sur une option entrée par l'utilisateur
 * 
 */
typedef struct param_item_s{
    bool exist;/**< présence de l'option*/
    char_tab_dyna option;/**< option entrée par l'utilisateur*/

}param_item;

/**
 * @brief structure ayant pour rôle  de stocker les parametre
 * 
 */
typedef struct parametre_s {
    param_item i;/**< objet définissent le parametre i*/
    param_item d;/**< objet définissent le parametre d*/
    param_item o;/**< objet définissent le parametre o*/
    param_item m;/**< objet définissent le parametre m*/
} parametre;


/**
 * @brief assigne des valeurs aux parametres
 * 
 * @param[in] argc nb d'arguments
 * @param[in] argv liste arguments
 * @param[out] param paramètre a définir
 */
void set_param(int argc,const char* argv[], parametre *param);

/**
 * @brief initialise les parametres
 * 
 * @param[out] param parametra a initialisé
 */
void init_param(parametre* param);

/**
 * @brief initialise un param item
 * 
 * @param[out] item parametre a initialisé
 */
void init_param_item(param_item *item);

/**
 * @brief assigne des valeurs aux parametre_item
 *
 * @param[out] item parametre a mofifier
 * @param[in] option option entré par l'utilisateur 
 */
void set_param_item(param_item *item, const char* option);

/**
 * @brief si une option a été précisé par l'utilisateur
 * 
 * @param[in] item option a vérifier
 * @return true a été entré
 * @return false n'a pas été précisé
 */
bool option_exist(param_item item);

/**
 * @brief obtenir l'option entré sous forme de
 * 
 * @param[in] item param item 
 * @return const char* string correspondant
 */
const char* get_string_option(param_item item);

/**
 * @brief si un string corredpond a une balise de parametre
 * 
 * @param[in] str 
 * @return true 
 * @return false 
 */
bool est_option(const char* str);

/**
 * @brief si la valeur d'un item correspond a une option de m
 * 
 * @param[in] item 
 * @return true 
 * @return false 
 */
bool is_m_option(param_item item);

/**
 * @brief si une option d'item se fini par une extension spécifié sous format ".extension"
 * 
 * @param[in] item 
 * @param[in] extension
 * @return true 
 * @return false 
 */
bool is_specified_format(param_item item, char* extension);

/**
 * @brief si les argument entré par l'utilisateur sont cohérent 
 * 
 * @param[in] param 
 * @return true 
 * @return false 
 */
bool argument_param_valide(parametre param);

/**
 * @brief si le format des paramètres entré est valable 
 * 
 * @param[in] argc 
 * @param[in] argv 
 * @return true 
 * @return false 
 */
bool format_param_valide(int argc,const char* argv[]);



#endif

