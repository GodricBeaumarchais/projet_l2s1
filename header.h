/**
 * @file        header.h
 * @brief       Contiens toutes les déclarations du Projet
 * @author      Maxime Tancrède
 * @version     0.1
 * @date        novembre 2021
 * @copyright   GNU Public License.
 */

#ifndef HEADER_H
#define HEADER_H

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#endif